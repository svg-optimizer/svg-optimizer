const url = require("url");
const path = require("path");
const electron = require("electron");
const app = electron.app;
const Menu = electron.Menu;
const ipc = electron.ipcMain;
const BrowserWindow = electron.BrowserWindow;

let win;

// create upload file window ####################################################################################################//

function createUploadFileWindow() {
    win.loadURL(url.format({
        pathname: path.join(__dirname,"src","upload_file","upload_file.html"),
        protocol: "file",
        slashes: true
    }));

    win.on("closed", () => {win = null;});
}

// create svg analyzer window ####################################################################################################//

function createSvgAnalyzerWindow(inputPathList,outputPath,svgAnalyzerObject) {
    win.loadURL(url.format({
        pathname: path.join(__dirname,"src","svg_analyzer","svg_analyzer.html"),
        protocol: "file",
        slashes: true
    }));

    if(outputPath===undefined || outputPath===null) {
        outputPath = app.getPath("temp");
    }

    win.webContents.once('did-finish-load', function() {
        win.webContents.send("init",inputPathList,outputPath,svgAnalyzerObject);
    });
    
    win.on("closed", () => {win = null;});
}

// create svg optimiser window ####################################################################################################//

function createSvgOptimiserWindow(filePath,svgAnalyzerObject) {
    win.loadURL(url.format({
        pathname: path.join(__dirname,"src","svg_optimiser","svg_optimiser.html"),
        protocol: "file",
        slashes: true
    }));

    win.webContents.once('did-finish-load', function() {
        win.webContents.send("init",filePath,svgAnalyzerObject);
    });
    
    win.on("closed", () => {win = null;});
}

// ready, activate, window-all-closed ####################################################################################################//

app.on("ready", () => {
    win = getNewBrowserWindow();
    createUploadFileWindow();
});

app.on("activate", () => {
    if(win===null) {
        win = getNewBrowserWindow();
        createUploadFileWindow();
    }
});

app.on("window-all-closed", () => {
    if(process.platform !== "darwin") {
        app.quit();
    }
});

function getNewBrowserWindow() {
    return new BrowserWindow({
        width: 1024,
        height: 800,
        minWidth: 1024,
        minHeight: 600,
        webPreferences: {
            nodeIntegration: true
        }
    });
}

// ipc (Inter Process Communication)

ipc.on("open-upload-file-window",(event) => {
    createUploadFileWindow();
});

ipc.on("open-svg-analyzer-window",(event,inputPathList,outputPath,svgAnalyzerObject) => {
    createSvgAnalyzerWindow(inputPathList,outputPath,svgAnalyzerObject);
});

ipc.on("open-svg-optimiser-window",(event,filePath,svgAnalyzerObject) => {
    createSvgOptimiserWindow(filePath,svgAnalyzerObject);
});

// Application Menu

let template = [
    {
        label: "File", 
        submenu: [
            { role: 'quit' }
        ]
    }
]

const menu = Menu.buildFromTemplate(template)
Menu.setApplicationMenu(menu);