const path = require("path");
const fse = require("fs-extra");
const electron = require("electron");
const ipc = electron.ipcRenderer;
const app = electron.remote;
const dialog = app.dialog;

// init ####################################################################################################//

(function init() {
    let inputContainer = document.getElementsByClassName("upload")[0];
    inputContainer.onclick = (() => selectFileOrDirectory());

    inputContainer.ondragover = ((event) => overrideDefault(event));
    inputContainer.ondragenter = ((event) => overrideDefault(event));
    inputContainer.ondragleave = ((event) => overrideDefault(event));
    inputContainer.ondrop = ((event) => {overrideDefault(event);dragAndDropFileOrDirectory(event);});
}());

function overrideDefault(event) {
    event.preventDefault();
    event.stopPropagation();
}

// select file or directory ####################################################################################################//

function selectFileOrDirectory() {
    const options = {
        properties: ['showHiddenFiles','openDirectory','openFile','multiSelections'],
    };

    dialog.showOpenDialog(null,options,(response) => {
        if(response!==undefined) {
            upload(response);
        }
    }); 
}

// drag and drop file or directory ####################################################################################################//

function dragAndDropFileOrDirectory(event) {
    let dragAndDropInput = event.dataTransfer.files;
    if(dragAndDropInput.length!=0) {
        let pathList = [];
        for(var position=0;position<dragAndDropInput.length;position++) {
            pathList.push(dragAndDropInput[position].path);
        }
        upload(pathList);
    }
}

// upload file or directory ####################################################################################################//

function upload(pathList) {
    let validPathList = getValidPathList(pathList);

    if(validPathList.length===0) {
        alert("Please upload SVG Files OR Folders");
    } else if(validPathList.length===1 && path.extname(validPathList[0])===".svg") {
        ipc.send("open-svg-optimiser-window",validPathList[0]);
    } else {
        ipc.send("open-svg-analyzer-window",validPathList);
    }
}

function getValidPathList(pathList) {
    let validPathList = [];

    pathList.forEach((filePath) => {
        let fileStat = fse.statSync(filePath);
        if(fileStat.isDirectory())
            validPathList.push(filePath);
        else if(path.extname(filePath)===".svg")
            validPathList.push(filePath);
    });

    return validPathList;
}