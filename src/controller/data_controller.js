let inputPathList = [];
let outputPath = null;
let svgFilesList = [];

let searchFilter = "";
let showSvgData = false;
let showOnlyRiskySvg = false;
let threshold = { size:24, domCount:32 };

const SORT_BY_NAME = 0;
const SORT_BY_ORIGINAL_SIZE = 1;
const SORT_BY_OPTIMISED_SIZE = 2;
const SORT_BY_SIZE_GAIN = 3;
const SORT_BY_ORIGINAL_DOM_COUNT = 4;
const SORT_BY_OPTIMISED_DOM_COUNT = 5;
const SORT_BY_DOM_COUNT_GAIN = 6;

// input path list ####################################################################################################//

module.exports.getInputPathList = function() {
    return inputPathList;
}

module.exports.setInputPathList = function(currentInputPathList) {
    inputPathList = currentInputPathList;
}

// output path ####################################################################################################//

module.exports.getOutputPath = function() {
    return outputPath;
}

module.exports.setOutputPath = function(currentOutputPath) {
    outputPath = currentOutputPath;
}

// search filter ####################################################################################################//

module.exports.getSearchFilter = function() {
    return searchFilter;
}

module.exports.setSearchFilter = function(currentSearchFilter) {
    searchFilter = currentSearchFilter;
}

// show svg data ####################################################################################################//

module.exports.getShowSvgData = function() {
    return showSvgData;
}

module.exports.setShowSvgData = function(currentShowSvgData) {
    showSvgData = currentShowSvgData;
}

// show only risky svg ####################################################################################################//

module.exports.getShowOnlyRiskySvg = function() {
    return showOnlyRiskySvg;
}

module.exports.setShowOnlyRiskySvg = function(currentShowOnlyRiskySvg) {
    showOnlyRiskySvg = currentShowOnlyRiskySvg;
}

// threshold ####################################################################################################//

module.exports.getThreshold = function() {
    return threshold;
}

module.exports.setThreshold = function(currentThreshold) {
    threshold = currentThreshold;
}

// svg files list ####################################################################################################//

module.exports.getSvgFilesList = function() {
    return svgFilesList;
}

module.exports.setSvgFilesList = function(currentSvgFilesList) {
    svgFilesList = currentSvgFilesList;
}

module.exports.clearSvgFilesList = function() {
    svgFilesList = [];
}

module.exports.addSvgFileToList = function(svgFile) {
    svgFilesList.push(svgFile);
}

module.exports.getFilteredSvgFilesList = function() {
    let currentSearchFilter = searchFilter.toLowerCase();
    return svgFilesList.filter((svgFile) => {
        if(svgFile.fileName.toLowerCase().includes(currentSearchFilter)) {
            if(showOnlyRiskySvg===false)
                return true;
            else if(bytesToKb(svgFile.originalSize)>threshold.size || svgFile.originalDomCount>threshold.domCount)
                return true;
        }
        return false;
    });
}

module.exports.getFilteredSvgFilesListAndStat = function() {
    
    let svgFilesListStat = {
        totalSvgFiles: 0,
        svgFilesAboveThresholdSize: 0,
        svgFilesAboveThresholdDomCount: 0,
    
        totalOriginalSize: 0,
        totalOriginalDomCount: 0,
        
        totalOptimisedSize: 0,
        totalOptimisedDomCount: 0,
    };

    let filteredSvgFilesList = DATA_CONTROLLER.getFilteredSvgFilesList();
    svgFilesListStat.totalSvgFiles = filteredSvgFilesList.length;
    filteredSvgFilesList.forEach((svgFile) => {
        
        if(bytesToKb(svgFile.originalSize)>threshold.size) 
            svgFilesListStat.svgFilesAboveThresholdSize++;
        if(svgFile.originalDomCount>threshold.domCount)
            svgFilesListStat.svgFilesAboveThresholdDomCount++;

        svgFilesListStat.totalOriginalSize += svgFile.originalSize;
        svgFilesListStat.totalOriginalDomCount += svgFile.originalDomCount;
        
        svgFilesListStat.totalOptimisedSize += svgFile.optimisedSize;
        svgFilesListStat.totalOptimisedDomCount += svgFile.optimisedDomCount;
    });

    return {
        filteredSvgFilesList: filteredSvgFilesList,
        svgFilesListStat: svgFilesListStat
    };
}

module.exports.sortSvgFilesList = function(sortBy) {
    
    if(isUndefinedOrNull(sortBy)===true) {
        sortBy = 0;   
    }

    if(sortBy===SORT_BY_NAME) {
        svgFilesList.sort((x,y) => {
            let xName = x.fileName.toLowerCase();
            let yName = y.fileName.toLowerCase();
            if(xName<yName)
                return -1;
            else
                return 1;
        });
    } else if(sortBy===SORT_BY_ORIGINAL_SIZE) {
        svgFilesList.sort((x,y) => {
            return y.originalSize-x.originalSize;
        });   
    } else if(sortBy===SORT_BY_OPTIMISED_SIZE) {
        svgFilesList.sort((x,y) => {
            return y.optimisedSize-x.optimisedSize;
        });
    } else if(sortBy===SORT_BY_SIZE_GAIN) {
        svgFilesList.sort((x,y) => {
            let xGain = x.originalSize-x.optimisedSize;
            let yGain = y.originalSize-y.optimisedSize;
            return yGain-xGain;
        });
    } else if(sortBy===SORT_BY_ORIGINAL_DOM_COUNT) {
        svgFilesList.sort((x,y) => {
            return y.originalDomCount-x.originalDomCount;
        });
    } else if(sortBy===SORT_BY_OPTIMISED_DOM_COUNT) {
        svgFilesList.sort((x,y) => {
            return y.optimisedDomCount-x.optimisedDomCount;
        });
    } else if(sortBy===SORT_BY_DOM_COUNT_GAIN) {
        svgFilesList.sort((x,y) => {
            let xGain = x.originalDomCount-x.optimisedDomCount;
            let yGain = y.originalDomCount-y.optimisedDomCount;
            return yGain-xGain;
        });
    }
}

// helper function ####################################################################################################//

function isUndefinedOrNull(x) {
    if(x===undefined || x===null)
        return true;
    else
        return false;
}

function bytesToKb(numberOfBytes) {
    return (numberOfBytes/1024).toFixed(2);
}