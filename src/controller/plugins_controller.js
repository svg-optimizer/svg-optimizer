module.exports.getPluginsList = function() {
    return defaultPlugins;
}

module.exports.setPluginsList = function(currentPlugins) {
    defaultPlugins = currentPlugins;
}

module.exports.resetPlugins = function() {
    defaultPlugins.forEach((plugin) => {
        plugin.currentValue = plugin.defaultValue;
    });
}

module.exports.setPlugin = function(position,plugin) {
    defaultPlugins[position] = plugin;
}

module.exports.setPluginValue = function(position,value) {
    defaultPlugins[position].currentValue = value;
}

module.exports.togglePluginValue = function(position) {
    defaultPlugins[position].currentValue = ! defaultPlugins[position].currentValue;
}

module.exports.getPluginsConfig = function() {
    let currentPlugin = [];
    defaultPlugins.forEach(function(plugin) {
        let attr = {};
        attr[plugin.id] = plugin.currentValue;
        currentPlugin.push(attr);
    });
    return {plugins: currentPlugin};
}

module.exports.getPluginHtmlView = function(plugin,position,callback) {
    
    let checkboxContainer = document.createElement("div");
    checkboxContainer.classList.add("plugin");
    checkboxContainer.classList.add("custom-control");
    checkboxContainer.classList.add("custom-switch");
    checkboxContainer.setAttribute("data-toggle","tooltip");
    checkboxContainer.setAttribute("title",plugin.info);
    
    if(isUndefinedOrNull(callback)===false) {
        checkboxContainer.onchange = (() => callback(position));
    }

    checkboxContainer.innerHTML =   `   <input type="checkbox" class="custom-control-input cursor-pointer" id="customSwitch${position}" ${plugin.currentValue? "checked":""}>
                                        <label class="custom-control-label cursor-pointer" for="customSwitch${position}">${plugin.title}</label>
                                    `;

    return checkboxContainer;
}


function isUndefinedOrNull(x) {
    if(x===undefined || x===null) 
        return true;
    else
        return false;
}

// svgo plugins ####################################################################################################//

let defaultPlugins =  [   {     id: "cleanupAttrs",
                                title: "Clean Attributes",
                                info: "Cleanups attributes from newlines, trailing and repeating spaces",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "cleanupEnableBackground",
                                title: "Cleanup Enable Background",
                                info: "Remove or cleanup enable-background attribute when possible",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "cleanupIDs",
                                title: "Cleanup IDs",
                                info: "Removes unused IDs and minifies used",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "cleanupListOfValues",
                                title: "Cleanup List Of Values",
                                info: "Rounds list of values to the fixed precision",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "cleanupNumericValues",
                                title: "Cleanup Numeric Values",
                                info: "Rounds numeric values to the fixed precision, removes default ‘px’ units",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "collapseGroups",
                                title: "Collapse Groups",
                                info: "Collapses useless groups",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "convertColors",
                                title: "Convert Colors",
                                info: "Converts colors: rgb() to #rrggbb and #rrggbb to #rgb",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "convertPathData",
                                title: "Convert Path Data",
                                info: "Optimizes path data: writes in shorter form, applies transformations",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "convertShapeToPath",
                                title: "Convert Shape To Path",
                                info: "Converts basic shapes to more compact path form",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "convertStyleToAttrs",
                                title: "Convert Style To Attributes",
                                info: "Converts style to attributes",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "convertTransform",
                                title: "Convert Transform",
                                info: "Collapses multiple transformations and optimizes it",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "inlineStyles",
                                title: "Inline Styles",
                                info: "Inline styles (additional options)",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "mergePaths",
                                title: "Merge Paths",
                                info: "Merges multiple paths in one if possible",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "minifyStyles",
                                title: "Minify Styles",
                                info: "Minifies styles and removes unused styles based on usage data",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "moveElemsAttrsToGroup",
                                title: "Move Element Attributes To Group",
                                info: "moves elements attributes to the existing group wrapper",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "moveGroupAttrsToElems",
                                title: "Move Group Attributes To Elements",
                                info: "moves some group attributes to the content elements",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "prefixIds",
                                title: "Prefix Ids",
                                info: "prefix IDs",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "removeComments",
                                title: "Removes Comments",
                                info: "removes comments",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "removeDesc",
                                title: "Removes 'desc' tag",
                                info: "Removes <desc>",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "removeDimensions",
                                title: "Remove Dimensions",
                                info: "Removes width and height in presence of viewBox (opposite to removeViewBox, disable it first)",
                                currentValue: false,
                                defaultValue: false
                            },
                            {   id: "removeDoctype",
                                title: "Remove Doctype",
                                info: "Removes doctype declaration",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "removeEditorsNSData",
                                title: "Removes Editor NSDATA",
                                info: "Removes editors namespaces, elements and attributes",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "removeEmptyAttrs",
                                title: "Remove Empty Attributes",
                                info: "Removes empty attributes",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "removeEmptyContainers",
                                title: "Remove Empty Containers",
                                info: "removes empty container elements",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "removeEmptyText",
                                title: "Remove Empty Text",
                                info: "Removes empty <text> elements",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "removeHiddenElems",
                                title: "Remove Hidden Elements",
                                info: "Removes hidden elements (zero sized, with absent attributes)",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "removeMetadata",
                                title: "Remove Metadata",
                                info: "Removes <metadata>",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "removeNonInheritableGroupAttrs",
                                title: "Remove Non Inheritable Group Attributes",
                                info: "Removes non-inheritable group’s presentational attributes",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "removeOffCanvasPaths",
                                title: "Remove Off Canvas Paths",
                                info: "Removes elements that are drawn outside of the viewbox (disabled by default)",
                                currentValue: false,
                                defaultValue: false
                            },
                            {   id: "removeRasterImages",
                                title: "Remove Raster Images",
                                info: "Removes raster images (disabled by default)",
                                currentValue: false,
                                defaultValue: false
                            },
                            {   id: "removeScriptElement",
                                title: "Remove Script Element",
                                info: "Removes <script> elements (disabled by default)",
                                currentValue: false,
                                defaultValue: false
                            },
                            {   id: "removeStyleElement",
                                title: "Remove Style Element",
                                info: "Removes <style> element (disabled by default)",
                                currentValue: false,
                                defaultValue: false
                            },
                            {   id: "removeTitle",
                                title: "Removes title",
                                info: "Removes <title>",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "removeUnknownsAndDefaults",
                                title: "Remove Unknowns And Defaultse",
                                info: "Removes unknown elements content and attributes, removes attrs with default values",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "removeUnusedNS",
                                title: "Remove Unused NS",
                                info: "Removes unused namespaces declaration",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "removeUselessDefs",
                                title: "Remove Useless Defs",
                                info: "Removes elements in <defs> without id",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "removeUselessStrokeAndFill",
                                title: "Remove Useless Stroke And Fill",
                                info: "Removes useless stroke and fill attributes",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "removeViewBox",
                                title: "Remove View Box",
                                info: "Removes viewBox attribute when possible",
                                currentValue: false,
                                defaultValue: false
                            },
                            {   id: "removeXMLNS",
                                title: "Remove XMLNS",
                                info: "Removes xmlns attribute (for inline svg, disabled by default)",
                                currentValue: false,
                                defaultValue: false
                            },
                            {   id: "removeXMLProcInst",
                                title: "Removes XML Processing Instructions",
                                info: "Removes XML processing instructions",
                                currentValue: true,
                                defaultValue: true
                            },
                            {   id: "reusePaths",
                                title: "Reuse Path",
                                info: "Finds <path> elements with the same d, fill, and stroke, and converts them to <use> elements referencing a single <path> def",
                                currentValue: false,
                                defaultValue: false
                            },
                            {   id: "sortAttrs",
                                title: "Sort Attributes",
                                info: "Sorts element atributes (disabled by default)",
                                currentValue: false,
                                defaultValue: false
                            },
                    ];
