module.exports.isUndefinedOrNull = function(x) {
    if(x===undefined || x===null) 
        return true;
    else
        return false;
}

module.exports.bytesToKb = function(numberOfBytes) {
    return (numberOfBytes/1024).toFixed(2);
}

module.exports.getDomCount = function(fileContent) {
    
    let match1 = fileContent.match(/<[^>]+>/g);
    let match2 = fileContent.match(/<\//g);

    let domCount = 0;
    if(isUndefinedOrNull(match1)===false)
        domCount += match1.length;
    if(isUndefinedOrNull(match2)===false)
        domCount -= match2.length;

    return domCount;
}

module.exports.printError = function(err) {
    console.log("ERROR:",err);
    alert(err);
}
