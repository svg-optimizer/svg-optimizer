const path = require("path");
const fse = require("fs-extra");
const electron = require("electron");
const ipc = electron.ipcRenderer;
const app = electron.remote;
const dialog = app.dialog;

// methods
const SVGO = require("../../node_modules/svgo/lib/svgo");
const DATA_CONTROLLER = require("../controller/data_controller.js");
const PLUGINS_CONTROLLER = require("../controller/plugins_controller.js");
const { isUndefinedOrNull, bytesToKb, getDomCount, printError } = require("../controller/helper_function");

// init ####################################################################################################//

ipc.on("init",(event,inputPathList,outputPath,svgAnalyzerObject) => {
    
    DATA_CONTROLLER.setInputPathList(inputPathList);
    DATA_CONTROLLER.setOutputPath(outputPath);

    setTopNavigationBar();
    setSvgDataContainer();
    readStoredData();

    if(isUndefinedOrNull(svgAnalyzerObject)===true) {
        optimiseInputPathList();
    } else {
        
        DATA_CONTROLLER.setSvgFilesList(svgAnalyzerObject.svgFilesList);
        DATA_CONTROLLER.setThreshold(svgAnalyzerObject.threshold);
        DATA_CONTROLLER.setSearchFilter(svgAnalyzerObject.searchFilter);
        DATA_CONTROLLER.setShowSvgData(svgAnalyzerObject.showSvgData);
        DATA_CONTROLLER.setShowOnlyRiskySvg(svgAnalyzerObject.showOnlyRiskySvg);
        
        setSearchFilter();
        setShowSvgData();
        setShowOnlyRiskySvg();
        
        createHtmlView();
        scrollToPosition(svgAnalyzerObject.currentPosition);
    }
});

function scrollToPosition(previousPosition) {
    window.scrollTo(previousPosition.x,previousPosition.y);
}

// optimise input path list ####################################################################################################//

function optimiseInputPathList() {
    
    let outputPath = path.join(DATA_CONTROLLER.getOutputPath(),"optimised_svg");
    
    fse.remove(outputPath,(err) => {
        if(err) {
            printError(err);
        } else {
            let svgo = new SVGO(PLUGINS_CONTROLLER.getPluginsConfig());

            let promiseList = [];
            DATA_CONTROLLER.getInputPathList().forEach((inputPath) => {
                optimiseInputPathRecursively(svgo,inputPath,path.join(outputPath,path.basename(inputPath)),promiseList);
            });

            Promise.all(promiseList).then((svgFilesList) => {
                DATA_CONTROLLER.setSvgFilesList(svgFilesList);
                DATA_CONTROLLER.sortSvgFilesList();
                createHtmlView();    
            });
        }
    });
}

function optimiseInputPathRecursively(svgo,inputPath,outputPath,promiseList) {

    let originalFileStat = fse.statSync(inputPath);
    if(originalFileStat.isDirectory()===true) {
        let filesList = fse.readdirSync(inputPath);
        filesList.forEach((fileName) => {
            let originalFilePath = path.join(inputPath,fileName);
            let optimisedFilePath = path.join(outputPath,fileName);
            optimiseInputPathRecursively(svgo,originalFilePath,optimisedFilePath,promiseList);
        });
    } else if(path.extname(inputPath)===".svg") {
        let originalFileContent = fse.readFileSync(inputPath,'utf8');
        promiseList.push(new Promise((resolve,reject) => {
            svgo.optimize(originalFileContent, {path: inputPath}).then((optimisedFileContent) => {
                fse.outputFileSync(outputPath,optimisedFileContent.data,'utf8');
                resolve({
                    fileName: path.basename(inputPath),

                    originalPath: inputPath,
                    originalSize: Buffer.byteLength(originalFileContent,'utf8'),
                    originalDomCount: getDomCount(originalFileContent),

                    optimisedPath: outputPath,
                    optimisedSize: Buffer.byteLength(optimisedFileContent.data,'utf8'),
                    optimisedDomCount: getDomCount(optimisedFileContent.data)
                });
            });
        }));
    }
}

// create html view ####################################################################################################//

function createHtmlView() {

    let filteredSvgFilesListAndStat = DATA_CONTROLLER.getFilteredSvgFilesListAndStat();

    setSvgFilesListStat(filteredSvgFilesListAndStat.svgFilesListStat);
    addAllSvgFilesToHtml(filteredSvgFilesListAndStat.filteredSvgFilesList);
    
    if(filteredSvgFilesListAndStat.svgFilesListStat.totalSvgFiles===0)
        showPageContent(false);
    else
        showPageContent(true);
}

function setSvgFilesListStat(svgFilesListStat) {
    let sizeReduced = svgFilesListStat.totalOriginalSize - svgFilesListStat.totalOptimisedSize;
    let domCountReduced = svgFilesListStat.totalOriginalDomCount - svgFilesListStat.totalOptimisedDomCount;

    let percentageSizeReduced = 0;
    if(svgFilesListStat.totalOriginalSize!=0) {
        percentageSizeReduced = ((sizeReduced*100)/svgFilesListStat.totalOriginalSize).toFixed(2);
    }

    let percentageDomCountReduced = 0;
    if(svgFilesListStat.totalOriginalDomCount!=0) {
        percentageDomCountReduced = ((domCountReduced*100)/svgFilesListStat.totalOriginalDomCount).toFixed(2);
    }

    document.getElementsByClassName("total-svg-files")[0].innerHTML = `${svgFilesListStat.totalSvgFiles}`;
    document.getElementsByClassName("svg-files-above-threshold-size")[0].innerHTML = `${svgFilesListStat.svgFilesAboveThresholdSize}`;
    document.getElementsByClassName("svg-files-above-threshold-dom-count")[0].innerHTML = `${svgFilesListStat.svgFilesAboveThresholdDomCount}`;

    document.getElementsByClassName("total-original-size")[0].innerHTML = `${bytesToKb(svgFilesListStat.totalOriginalSize)} KB`;
    document.getElementsByClassName("total-optimised-size")[0].innerHTML = `${bytesToKb(svgFilesListStat.totalOptimisedSize)} KB`;
    document.getElementsByClassName("size-reduced")[0].innerHTML = `${bytesToKb(sizeReduced)} KB (~${percentageSizeReduced}%)`;

    document.getElementsByClassName("total-original-dom-count")[0].innerHTML = `${svgFilesListStat.totalOriginalDomCount} DOMs`;
    document.getElementsByClassName("total-optimised-dom-count")[0].innerHTML = `${svgFilesListStat.totalOptimisedDomCount} DOMs`;
    document.getElementsByClassName("dom-count-reduced")[0].innerHTML = `${domCountReduced} DOMs (~${percentageDomCountReduced}%)`;
}

function addAllSvgFilesToHtml(filteredSvgFilesList) {
    
    let threshold = DATA_CONTROLLER.getThreshold();
    let showSvgData = DATA_CONTROLLER.getShowSvgData();
    let svgFilesContainer = document.getElementsByClassName("svg-container")[0];

    svgFilesContainer.innerHTML = "";
    filteredSvgFilesList.forEach((svgFile) => {
        svgFilesContainer.append(getSvgHtml(svgFile,threshold,showSvgData));
    });
}

function getSvgHtml(svgFile,threshold,showSvgData) {

    let sizeReduced = svgFile.originalSize - svgFile.optimisedSize;
    let domCountReduced = svgFile.originalDomCount - svgFile.optimisedDomCount;
    
    let percentageSizeReduced = 0;
    if(svgFile.originalSize!=0) {
        percentageSizeReduced = ((sizeReduced*100)/svgFile.originalSize).toFixed(2);
    }
    
    let percentageDomCountReduced = 0;
    if(svgFile.originalDomCount!=0) {
        percentageDomCountReduced = ((domCountReduced*100)/svgFile.originalDomCount).toFixed(2)
    }

    let svgContainer = document.createElement("article");
    if(bytesToKb(svgFile.originalSize)>threshold.size || svgFile.originalDomCount>threshold.domCount) {
        svgContainer.classList.add("risky");
    }
    
    if(showSvgData===true) {
        svgContainer.classList.add("big-article");
        svgContainer.innerHTML = `
            <img class="open-optimiser cursor-pointer" src="../assets/open_optimiser.svg">
            <img class="svg-file" src="${svgFile.optimisedPath}">
            <p class="svg-name default-pointer">Name: ${svgFile.fileName}</p>
            
            <hr>

            <p class="svg-original-size default-pointer">Original Size: ${bytesToKb(svgFile.originalSize)} KB</p>
            <p class="svg-optimised-size default-pointer">Optimised Size: ${bytesToKb(svgFile.optimisedSize)} KB</p>
            <p class="svg-size-reduced default-pointer">Size Reduced: ${bytesToKb(sizeReduced)} KB (~${percentageSizeReduced}%)</p>
            
            <hr>

            <p class="svg-original-dom-count default-pointer">Original DOM Elements: ${svgFile.originalDomCount}</p>
            <p class="svg-optimised-dom-count default-pointer">Optimised DOM Elements: ${svgFile.optimisedDomCount}</p>
            <p class="svg-dom-count-reduced default-pointer">DOM Elements Reduced: ${domCountReduced} (~${percentageDomCountReduced}%)</p>
        `;
    } else {
        svgContainer.classList.add("small-article");
        svgContainer.innerHTML = `
            <img class="open-optimiser cursor-pointer" src="../assets/open_optimiser.svg">
            <img class="svg-file" src="${svgFile.optimisedPath}">
            <p class="svg-name default-pointer">Name: ${svgFile.fileName}</p>
        `;
    }

    svgContainer.getElementsByClassName("open-optimiser")[0].onclick = (() => openSvgOptimiserWindow(event,svgFile.originalPath));

    return svgContainer;
}

function openSvgOptimiserWindow(event,filePath) {
    event.stopPropagation();

    ipc.send("open-svg-optimiser-window",filePath,{
        inputPathList: DATA_CONTROLLER.getInputPathList(),
        outputPath: DATA_CONTROLLER.getOutputPath(),

        svgFilesList: DATA_CONTROLLER.getSvgFilesList(),
        threshold: DATA_CONTROLLER.getThreshold(),
        searchFilter: DATA_CONTROLLER.getSearchFilter(),
        showSvgData: DATA_CONTROLLER.getShowSvgData(),
        showOnlyRiskySvg: DATA_CONTROLLER.getShowOnlyRiskySvg(),
        currentPlugins: PLUGINS_CONTROLLER.getPluginsList(),
        currentPosition: {x:window.pageXOffset, y:window.pageYOffset}
    });
}   

function showPageContent(containsSvg) {
    
    if(containsSvg===true) {
        document.getElementsByClassName("svg-container")[0].classList.remove("hide");
        document.getElementsByClassName("no-svg-alert")[0].classList.add("hide");
    } else {
        document.getElementsByClassName("svg-container")[0].classList.add("hide");
        document.getElementsByClassName("no-svg-alert")[0].classList.remove("hide");    
    }
    document.getElementsByClassName("analyzer-container")[0].classList.remove("hide");
    document.getElementsByClassName("loading-container")[0].classList.add("hide");
    document.getElementsByClassName("setting-container")[0].classList.add("hide");
}

// top navigation bar ####################################################################################################//

function setTopNavigationBar() {
    
    // back
    document.getElementsByClassName("back")[0].onclick = (() => onBackClicked());

    // setting
    document.getElementsByClassName("setting")[0].onclick = (() => onSettingClicked());

    // download
    document.getElementsByClassName("download")[0].onclick = (() => downloadOptimisedFiles());

    // replace
    document.getElementsByClassName("replace")[0].onclick = (() => replaceOriginalFiles());

    // search
    document.getElementsByClassName("search")[0].onchange = (() => applySearch());

    // sort-by in data-container
    let sortByOptions = document.getElementsByClassName("sort-by-option");
    for(let index=0;index<sortByOptions.length;index++) {
        sortByOptions[index].onclick = (() => applySortBy(index));
    }
}

// back 

function onBackClicked() {
    let settingContainer = document.getElementsByClassName("setting-container")[0];
    if(settingContainer.classList.contains("hide")===true) {
        ipc.send("open-upload-file-window");
    } else {
        document.getElementsByClassName("setting-container")[0].classList.add("hide");
        document.getElementsByClassName("analyzer-container")[0].classList.remove("hide");
    }
}

// setting

function onSettingClicked() {
    let settingContainer = document.getElementsByClassName("setting-container")[0];
    if(settingContainer.classList.contains("hide")===true)
        openSetting();
    else
        onBackClicked();
}

// download

function downloadOptimisedFiles() {
    dialog.showSaveDialog({},(downloadPath) => {
        if(isUndefinedOrNull(downloadPath)===false) {
            fse.remove(downloadPath,(err) => {
                if(err) {
                    printError(err);
                } else {
                    let srcPath = path.join(DATA_CONTROLLER.getOutputPath(),"optimised_svg");
                    fse.copy(srcPath,downloadPath);
                }
            });
        }
    });
}

// replace

function replaceOriginalFiles() {
    let options = {
        type: 'question',
        buttons: ['Yes, please', 'No, thanks'],
        title: 'Question',
        message: 'Are you sure you want to replace original SVG files with optimised SVG files?',
    };
    dialog.showMessageBox(null, options, (response) => {
        if(response===0) {
            DATA_CONTROLLER.getInputPathList().forEach((inputPath) => {
                let srcPath = path.join(DATA_CONTROLLER.getOutputPath(),"optimised_svg",path.basename(inputPath));
                fse.copy(srcPath,inputPath);
            });
        }
    });
}

// search

function applySearch() {
    let currentSearchFilter = getSearchFilter();
    DATA_CONTROLLER.setSearchFilter(currentSearchFilter);
    createHtmlView();
}

// sort by 

function applySortBy(position) {
    
    let sortByOptions = document.getElementsByClassName("sort-by-option");
    for(let index=0;index<sortByOptions.length;index++) {
        sortByOptions[index].classList.remove("active");
    }
    sortByOptions[position].classList.add("active");

    DATA_CONTROLLER.sortSvgFilesList(position);
    createHtmlView();
}

// svg data container ####################################################################################################//

function setSvgDataContainer() {
    
    // show svg data
    document.getElementsByClassName("show-svg-data")[0].onchange = (() => applyShowSvgData());

    // show risky svg only 
    document.getElementsByClassName("show-only-risky-svg")[0].onchange = (() => applyShowOnlyRiskySvg());    
}

// show svg data

function applyShowSvgData() {
    let currentShowSvgData = getShowSvgData();
    DATA_CONTROLLER.setShowSvgData(currentShowSvgData);
    createHtmlView();
}

// show only risky svg

function applyShowOnlyRiskySvg() {
    let currentShowOnlyRiskySvg = getShowOnlyRiskySvg();
    DATA_CONTROLLER.setShowOnlyRiskySvg(currentShowOnlyRiskySvg);
    createHtmlView();
}

// svgo plugins setting ####################################################################################################//

function openSetting() {

    let analyzerContainer = document.getElementsByClassName("analyzer-container")[0];
    let settingContainer = document.getElementsByClassName("setting-container")[0];

    analyzerContainer.classList.add("hide");
    settingContainer.classList.remove("hide");

    setThreshold(DATA_CONTROLLER.getThreshold());
    displayPlugins();

    settingContainer.getElementsByClassName("apply-setting")[0].onclick = (() => applySetting());
    settingContainer.getElementsByClassName("reset-setting")[0].onclick = (() => resetSetting());
}

function displayPlugins() {
    let pluginContainer = document.getElementsByClassName("plugin-container")[0];
    pluginContainer.innerHTML = "";
    
    PLUGINS_CONTROLLER.getPluginsList().forEach(function(plugin,position) {
        pluginContainer.append(PLUGINS_CONTROLLER.getPluginHtmlView(plugin,position));
    });
}

// apply setting

function applySetting() {
    let currentThreshold = {};
    currentThreshold.size = Number(document.getElementsByClassName("threshold-size")[0].value);
    currentThreshold.domCount = Number(document.getElementsByClassName("threshold-dom-count")[0].value);
    DATA_CONTROLLER.setThreshold(currentThreshold);

    let settingContainer = document.getElementsByClassName("setting-container")[0];
    let pluginList = settingContainer.getElementsByClassName("plugin");
    PLUGINS_CONTROLLER.getPluginsList().forEach((plugin,position) => {
        PLUGINS_CONTROLLER.setPluginValue(position,pluginList[position].getElementsByTagName("input")[0].checked);
    });

    closeSetting();
    optimiseInputPathList();
}

// reset setting

function resetSetting() {
    let settingContainer = document.getElementsByClassName("setting-container")[0];
    let pluginList = settingContainer.getElementsByClassName("plugin");

    PLUGINS_CONTROLLER.resetPlugins();
    PLUGINS_CONTROLLER.getPluginsList().forEach((plugin,position) => {
        pluginList[position].getElementsByTagName("input")[0].checked = plugin.defaultValue; 
    });
}

function closeSetting() {
    document.getElementsByClassName("loading-container")[0].classList.remove("hide");
    document.getElementsByClassName("analyzer-container")[0].classList.add("hide");
    document.getElementsByClassName("setting-container")[0].classList.add("hide");
}

// helper function ####################################################################################################//

function getSearchFilter() {
    let searchFilterContainer = document.getElementsByClassName("search")[0];
    searchFilterContainer.blur();
    return searchFilterContainer.value;
}

function setSearchFilter() {
    let searchFilterContainer = document.getElementsByClassName("search")[0];
    searchFilterContainer.value = DATA_CONTROLLER.getSearchFilter();
}

function getShowSvgData() {
    let showSvgDataContainer = document.getElementsByClassName("show-svg-data-checkbox")[0];
    return showSvgDataContainer.checked;
}

function setShowSvgData() {
    let showSvgDataContainer = document.getElementsByClassName("show-svg-data-checkbox")[0];
    showSvgDataContainer.checked = DATA_CONTROLLER.getShowSvgData();
}

function getShowOnlyRiskySvg() {
    let onlyRiskySvgContainer = document.getElementsByClassName("show-only-risky-svg-checkbox")[0];
    return onlyRiskySvgContainer.checked;
}

function setShowOnlyRiskySvg() {
    let showOnlyRiskySvgContainer = document.getElementsByClassName("show-only-risky-svg-checkbox")[0];
    showOnlyRiskySvgContainer.checked = DATA_CONTROLLER.getShowOnlyRiskySvg();
}

function setThreshold(threshold) {
    document.getElementsByClassName("threshold-size")[0].value = threshold.size;
    document.getElementsByClassName("threshold-dom-count")[0].value = threshold.domCount;
}

// save and read settings ####################################################################################################//

document.addEventListener("visibilitychange", function() {
    if (document.hidden) {
        saveData();
    }
});

function saveData() {
    // save threshold
    localStorage.setItem("threshold",JSON.stringify(DATA_CONTROLLER.getThreshold()));

    // save plugins
    localStorage.setItem("plugins",JSON.stringify(PLUGINS_CONTROLLER.getPluginsList()));
}

function readStoredData() {
    // read threshold
    let threshold = localStorage.getItem("threshold");
    if(isUndefinedOrNull(threshold)===false) {
        threshold = JSON.parse(threshold);
        DATA_CONTROLLER.setThreshold(threshold);
    }

    // read plugins
    let plugins = localStorage.getItem("plugins");
    if(isUndefinedOrNull(plugins)===false) {
        plugins = JSON.parse(plugins);
        PLUGINS_CONTROLLER.setPluginsList(plugins);
    }
}
