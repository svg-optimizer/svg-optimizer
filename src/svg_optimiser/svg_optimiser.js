const path = require("path");
const fse = require("fs-extra");
const electron = require("electron");
const ipc = electron.ipcRenderer;
const app = electron.remote;
const dialog = app.dialog;

const SVGO = require("svgo/lib/svgo");
const PLUGINS_CONTROLLER = require("../controller/plugins_controller");
const { isUndefinedOrNull, bytesToKb, getDomCount, printError } = require("../controller/helper_function");

// variables
let SVG_ANALYZER_OBJECT = null;

// methods
let ORIGINAL_SVG_SIZE = 0;
let ORIGINAL_SVG_PATH = null;
let ORIGINAL_SVG_CONTENT = null;

// init ####################################################################################################//

ipc.on("init",(event,filePath,svgAnalyzerObject) => {
    
    SVG_ANALYZER_OBJECT = svgAnalyzerObject;

    if(isUndefinedOrNull(SVG_ANALYZER_OBJECT)===false) {
        PLUGINS_CONTROLLER.setPluginsList(getCopyOfCurrentPlugins(SVG_ANALYZER_OBJECT.currentPlugins));
    }

    fse.readFile(filePath,"UTF-8",(err,fileContent) => {
        ORIGINAL_SVG_PATH = filePath;
        ORIGINAL_SVG_CONTENT = fileContent;
        ORIGINAL_SVG_SIZE = Buffer.byteLength(ORIGINAL_SVG_CONTENT, 'utf8');

        setTopNavigationBar();
        setPluginsSetting();
        setSvgContainers();
        
        displayOriginalSvg();
        displayOptimisedSvg();
        
        addOpacityTransitionToAlert();
    });
});

function getCopyOfCurrentPlugins(plugins) {
    let currentPlugins = [];
    plugins.forEach((plugin) => {
        currentPlugins.push(JSON.parse(JSON.stringify(plugin)));
    });
    return currentPlugins;
}

function addOpacityTransitionToAlert() {
    let alertContainer = document.getElementsByClassName("alert");
    for(let index=0;index<alertContainer.length;index++) {
        alertContainer[index].classList.add("opacity-transition");
    }
}

// top navigation bar ####################################################################################################//

function setTopNavigationBar() {

    // back
    document.getElementsByClassName("back")[0].onclick = (() => onBack());

    // download
    document.getElementsByClassName("download")[0].onclick = (() => downloadOptimisedSvg());

    // replace
    document.getElementsByClassName("replace")[0].onclick = (() => replaceOriginalSvg());
}

// back

function onBack() {
    if(isUndefinedOrNull(SVG_ANALYZER_OBJECT)===false) {
        ipc.send("open-svg-analyzer-window",SVG_ANALYZER_OBJECT.inputPathList,SVG_ANALYZER_OBJECT.outputPath,SVG_ANALYZER_OBJECT);
    } else {
        ipc.send("open-upload-file-window");
    }
}

// download

function downloadOptimisedSvg() {
    getOptimisedSvg().then((optimisedSvgContent) => {
        dialog.showSaveDialog({ defaultPath: path.join(ORIGINAL_SVG_PATH,"..","optimised_"+path.basename(ORIGINAL_SVG_PATH)) },(downloadFilePath) => {
            if(isUndefinedOrNull(downloadFilePath)===false) {
                fse.writeFile(downloadFilePath,optimisedSvgContent);
            }
        });
    });
}

// replace

function replaceOriginalSvg() {
    getOptimisedSvg().then((optimisedSvgContent) => {
        let options = {
            type: 'question',
            buttons: ['Yes, please', 'No, thanks'],
            title: 'Question',
            message: 'Are you sure you want to replace original SVG file with optimised SVG file?',
        };
        dialog.showMessageBox(null, options, (response) => {
            if(response===0) {
                fse.writeFile(ORIGINAL_SVG_PATH,optimisedSvgContent);
            }
        });
    });
}

// svg containers ####################################################################################################//

function setSvgContainers() {
    // markup toggle 
    document.getElementsByClassName("original-svg-markup")[0].onchange = (() => displayOriginalSvg());
    document.getElementsByClassName("optimised-svg-markup")[0].onchange = (() => displayOptimisedSvg());

    // copy markup 
    document.getElementsByClassName("copy-original-svg")[0].onclick = (() => copyOriginalSvg());
    document.getElementsByClassName("copy-optimised-svg")[0].onclick = (() => copyOptimisedSvg());
}



function copyOriginalSvg() {
    let originalSvgMarkupContainer = document.createElement('input');
    originalSvgMarkupContainer.value = ORIGINAL_SVG_CONTENT;
    originalSvgMarkupContainer.classList.add("copy-svg-markup");
    document.body.appendChild(originalSvgMarkupContainer);
    originalSvgMarkupContainer.select();
    document.execCommand("copy");
    document.body.removeChild(originalSvgMarkupContainer);

    showOriginalSvgMarkupCopiedAlert();
}

function showOriginalSvgMarkupCopiedAlert() {

    let alertContainer = document.getElementsByClassName("original-svg-alert")[0];
    
    if(alertContainer.classList.contains("one-opacity")===false) {
        alertContainer.classList.add("one-opacity");
        setTimeout(function() { 
            alertContainer.classList.remove("one-opacity") 
        },1500);
    }
}

function copyOptimisedSvg() {
    getOptimisedSvg().then((optimisedSvgContent) => {
        let optimisedSvgMarkupContainer = document.createElement('input');
        optimisedSvgMarkupContainer.value = optimisedSvgContent;
        optimisedSvgMarkupContainer.classList.add("copy-svg-markup");
        document.body.appendChild(optimisedSvgMarkupContainer);
        optimisedSvgMarkupContainer.select();
        document.execCommand("copy");
        document.body.removeChild(optimisedSvgMarkupContainer);
    });

    showOptimisedSvgMarkupCopiedAlert();
}

function showOptimisedSvgMarkupCopiedAlert() {
    let alertContainer = document.getElementsByClassName("optimised-svg-alert")[0];
    if(alertContainer.classList.contains("one-opacity")===false) {
        alertContainer.classList.add("one-opacity");
        setTimeout(function() { 
            alertContainer.classList.remove("one-opacity") 
        },1500);
    }
}


// svg plugins setting ####################################################################################################//

function setPluginsSetting() {
    let settingContainer = document.getElementsByClassName("setting-container")[0];
    settingContainer.innerHTML = "";
    PLUGINS_CONTROLLER.getPluginsList().forEach(function(plugin,position) {
        settingContainer.appendChild(PLUGINS_CONTROLLER.getPluginHtmlView(plugin,position,onPluginChange));
    });
}

function onPluginChange(position) {
    PLUGINS_CONTROLLER.togglePluginValue(position);
    displayOptimisedSvg();
}

// display svg or svg markup ####################################################################################################//

// original svg

function displayOriginalSvg() {
    let originalSvgContainer = document.getElementsByClassName("original-svg-content")[0];
    let isMarkup = document.getElementsByClassName("original-svg-markup")[0].checked;
    if(isMarkup===false) {
        originalSvgContainer.classList.add("center");
        originalSvgContainer.innerHTML = ORIGINAL_SVG_CONTENT;
    } else {
        originalSvgContainer.classList.remove("center");
        let highlightedCode = Prism.highlight(ORIGINAL_SVG_CONTENT, Prism.languages.svg, 'svg');
        originalSvgContainer.innerHTML = `<pre><code class="language-svg">${highlightedCode}</code><pre>`;
    }
    displayOriginalSvgSize();
}

function displayOriginalSvgSize() {
    document.getElementsByClassName("original-size")[0].innerText = `Original Size : ${bytesToKb(ORIGINAL_SVG_SIZE)} KB`;
    document.getElementsByClassName("original-dom-count")[0].innerText = `Original DOM Elements : ${getDomCount(ORIGINAL_SVG_CONTENT)}`;
}

// optimised svg

function displayOptimisedSvg() {
    getOptimisedSvg().then((optimisedSvgContent) => {   
        let optimisedSvgContainer = document.getElementsByClassName("optimised-svg-content")[0];
        let isMarkup = document.getElementsByClassName("optimised-svg-markup")[0].checked;
        if(isMarkup===false) {
            optimisedSvgContainer.classList.add("center");
            optimisedSvgContainer.innerHTML = optimisedSvgContent;
        } else {
            optimisedSvgContainer.classList.remove("center");
            let highlightedCode = Prism.highlight(optimisedSvgContent, Prism.languages.svg, 'svg');
            optimisedSvgContainer.innerHTML = `<pre><code class="language-svg">${highlightedCode}</code><pre>`;
        }
        displayOptimisedSvgSize(optimisedSvgContent);
    });
}

function displayOptimisedSvgSize(optimisedSvgContent) {
    let optimisedSize = Buffer.byteLength(optimisedSvgContent, 'utf8');
    let optimisedDomCount = getDomCount(optimisedSvgContent);

    let optimisedSizeContainer = document.getElementsByClassName("optimised-size")[0];
    let optimisedDomCountContainer = document.getElementsByClassName("optimised-dom-count")[0];

    let optimisedSizeContent = `Optimised Size : ${bytesToKb(optimisedSize)} KB`;
    let optimisedDomCountContent = `Optimised DOM Elements : ${optimisedDomCount}`;

    let previousOptimisedSizeContent = optimisedSizeContainer.innerText;
    let previousOptimisedDomCountContent = optimisedDomCountContainer.innerText;

    optimisedSizeContainer.innerText = optimisedSizeContent;
    optimisedDomCountContainer.innerText = optimisedDomCountContent;

    if(previousOptimisedSizeContent!=optimisedSizeContent && previousOptimisedSizeContent!="") {
        optimisedSizeContainer.classList.add("bold");
        setTimeout(function() {
            optimisedSizeContainer.classList.remove("bold");
        },300);    
    }

    if(previousOptimisedDomCountContent!=optimisedDomCountContent && previousOptimisedDomCountContent!="") {
        optimisedDomCountContainer.classList.add("bold");
        setTimeout(function() {
            optimisedDomCountContainer.classList.remove("bold");
        },300);
    }
}

// helper function ####################################################################################################//

function getOptimisedSvg() {
    let svgo = new SVGO(PLUGINS_CONTROLLER.getPluginsConfig());
    return new Promise((resolve,reject) => {
        svgo.optimize(ORIGINAL_SVG_CONTENT, {input: 'string'}).then((result) => {
            resolve(result.data);
        });
    });
} 
